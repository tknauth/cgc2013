\section{Introduction}

%% Virtualization is common in today's data centers because it provides many benefits.
%% Security, isolation, ease of management~\cite{soundararajan2010impact}, scheduling flexibility~\cite{vogels2008beyond}.
%% Virtual machine migration has received a lot of attention in the past.
%% It is an established technique to dynamically change the workload assignment between physical servers.
%% Other virtual machine operations have received less attention.
%% Starting virtual machines from scratch takes on the order of minutes.
%% The startup time can be reduced by using a minimal virtual machine image.
%% A minimal image only contains the required packages, services, and devices.
%% This reduces the initialization time and decreases the attack surface for potential exploits [Delusional Boot, Eurosys 2012].

The benefits of Cloud computing are plentiful.
One of them is that enterprises, universities and other institutions must no longer buy their own IT infrastructure.
Instead of purchasing the equipment, it is rented from a Cloud provider.
This is especially attractive for IT startups as they can rarely predict whether their product will be a hit or miss.
If the startup succeeds in attracting users,
the IT infrastructure must scale quickly.
With Cloud computing the task of scaling the infrastructure is as easy as performing a few clicks in a web browser.

Infrastructure scaling as a response to a growing or shrinking user base is just one use case where the time frame in which new resources must be deployed or decommissioned is on the order of hours.
Another use case, where decisions must be made within seconds, is the handling of flash crowds.
To successfully combat flash crowds, resources must be provisioned as fast as possible, i.e., within a few \emph{seconds}.
Unfortunately, it takes up to several \emph{minutes} to deploy a new VM on today's Cloud providers~\cite{mao2012performance}.
One factor contributing to the startup latency is the VM's boot and initialization process.
The operating system and applications must be initialized before the VM starts servicing requests.

The lengthy startup phase can be circumvented by suspending VMs instead of shutting them down.
Suspending a VM is conceptually equivalent to a snapshot of the entire VM, including disk and memory.
When resuming the instance, all operating system services and applications are already initialized which reduces the time until the instance performs useful work.
While all modern virtualization technologies, i.e., VMWare, Xen, qemu, HyperV, offer this suspend/resume functionality,
Cloud providers, such as Amazon, currently do not expose it via their API.
We argue that exposing the suspend/resume operations to the Cloud customer makes sense because it enables faster resource provisioning than is possible with starting virtual machines from scratch.

While restoring a suspended VM potentially decreases the startup latency,
resuming still takes anywhere from a few seconds up to tens of seconds.
We have measured the resume time for the most recent version of the open-source emulator \emph{qemu} (Figure~\ref{fig:resume-times-unoptimized}).
The speed with which the VM is resumed mainly depends on the suspend image size:
it takes longer to read large images from disk.
Besides the resume image's size, the VM configuration also influences the resume speed.
VMs with more configured memory take longer to resume, even if the sizes of their suspend images are identical.

While resuming is still, in many cases, faster than starting from scratch,
there is clear potential to optimize the resume operation in \emph{qemu}.
The speed at which the VM is restored limits the flexibility of the suspend/resume operation.
If resuming a virtual machine takes tens of seconds,
users will avoid to suspend their virtual machines because of the latency penalty they pay on resume.
Especially for interactive services, e.g., a web server, users expect nearly instantaneous response times.
For example, is has been shown that for commercial web sites,
request latency is inversely related with revenue~\cite{latency-everywhere}.

%% In a Cloud environment, where Cloud customers pay for the resources they use,
%% it makes sense to suspend virtual machines if they are not needed.
%% Suspended VMs would be charged a smaller fee than a running virtual machine.
%% By explicitly suspending VM instances the Cloud customer can save money.
%% Current Cloud deployments, such as Amazon and RackSpace, do not offer this functionality.
%% One reason, why they expose no interface to suspend and resume VMs

%% \note{TK: two use cases (a) flash crowds and (b) on-demand services (per request VM)}

% \subsection{Contributions}

To summarize, our contributions are as follows:

\begin{itemize}
\item Motivate the need for why fast VM provisioning is important.
\item Identify potential to improve the resume speed in the open-source emulator qemu.
\item Prototypical implementation of a on-demand/lazy resume for qemu in less than 100 lines of additional code.
\item Compare our on-demand resume implementation against qemu's default resume implementation on a set of web-service benchmarks.
\item Share all code and data with the scientific community at \url{http://bitbucket.org/tknauth/cgc2013/}
\end{itemize}

\section{Problem}

Modern virtualization products typically allow three pairs of operations with respect to the life cycle management of virtual machines:
(i)~start/stop, (ii)~unpause/pause, (iii)~and resume/suspend.
The operations differ in two key aspects: their resource-consumption profile as well as the time it takes to perform the operation.
For the sake of completeness, we describe each operation briefly here.

Starting a virtual machine creates a new execution context from scratch.
It involves booting from a virtual machine image, enumerating all devices, and starting various operating system and application services.
The instance actively consumes resources as soon as it is started; until it is stopped.
Like stopping a physical machine, all services are shut down orderly.
After shutdown, the VM no longer actively consumes any resources.
The drawback of starting a VM from scratch is the high startup latency (compared to suspend/resume), e.g.,
it can take up to several minutes to spin up a new instance on EC2.
Part of this latency is due to the time spent initializing the OS, devices, and applications.
The ``startup cost'' is paid every time a new instance is commissioned.

An alternative to the starting and stopping a VM is to pause/unpause the VM.
Pausing a VM means temporarily halting it.
It no longer actively consumes any CPU cycles.
It does, however, still consume main memory.
Unpausing a virtual machine means giving it access to the CPU again.
Execution continues from the point where it was previously paused.
Pausing a VM can, for example, be used to quiesce a busy VM on an oversubscribed host.
By temporarily halting the virtual CPUs, interference with co-located VMs is reduced.
For longer periods of inactivity it is desirable to free up the RAM belonging to the VM.

If the VM instance is to be halted for a longer time period, say hours or days, it is sensible to also give back the VM's memory to the host.
To preserve the VM's in-memory state, the entire memory content is written to disk before terminating the virtual machine.
The persisted state allows to continue executing the VM anytime in the future.
In contrast to a paused instance, a suspended instance does not consume any CPU or memory resources.

The appeal of suspending and resuming virtual machines,
instead of booting from scratch,
is that resuming is generally faster because all the services, programs, and daemons are already initialized.
For example, in our experiments it takes 18 seconds from booting a new instance until it successfully responds to an HTTP request.
Compare this to the resume time in Figure~\ref{fig:resume-times-unoptimized} for instances with up to 4~GB of memory.
Even without any optimizations, qemu resumes VMs in less than 10 seconds (worst case) and little more than 2 seconds (best case).
Besides reducing the startup latency, valuable information,
e.g., about the working sets of programs running inside the VM,
is preserved across a suspend/resume cycle.

However, there is potential to provide even faster startup times with suspend/resume than is currently possible.
Figure~\ref{fig:resume-times-unoptimized} illustrates the time it takes to resume a VM based on the memory dump size.
%% The dump size varies based on the VM's memory usage.
%% For instances with identical memory configuration, e.g., 4~GB,
%% the dump size varies based on the VM's memory usage pattern.
%% When a VM with many unused pages is suspended,
%% the unsused pages are not written to disk.
%% Similarly, pages with a single, repeated byte pattern are compressed to also save disk space.
We observe two things in Figure~\ref{fig:resume-times-unoptimized}:
First, the resume time increases linearly with the dump size.
For larger dump sizes the resume time is determined by the rate at which data is transferred from disk to memory.
One solution to decrease the VM resume time is to increase the disk bandwidth.
Investing in higher grade hardware, however, is expensive.
Which is why we prefer a software solution.
The second conclusion we draw from Figure~\ref{fig:resume-times-unoptimized} is that the resume time also depends on the VM's configured memory,
i.e., the resume time is, to a degree, independent of the dump size.
For example, resuming a VM with 500~MB of state takes 2, 3, or 5 seconds for an instance configured with 1, 2, or 4~GB of memory, respectively~\footnote{It is not supported to suspend a 1~GB VM and resume it as a 2~GB VM.}.
This indicates that the resume speed is not only determined by the dump size,
but also by the VM's memory configuration.

\begin{figure}
  \includegraphics{figs/1364119326-resume}
\caption{Virtual machine resume time as a function of memory dump size.
The dump size does not equal the configured memory size because, for example, zero pages are no explicitly stored in the dump.
The measurements were conducted with a cold page cache, i.e.,
the system had to read the entire dump from stable storage.}
\label{fig:resume-times-unoptimized}
\end{figure}

Because the resume time is directly linked to the dump size,
it pays off to read as little state as possible from disk to speed up the resume time.
Hence, we propose to implement an on-demand VM resume feature for qemu.
The key insight here is that re-starting the virtual machine does not require all of the VM's memory to be read from disk.
Instead, memory can be read on-demand, based on the memory access pattern inside the VM.
As we will see in our evaluation,
the on-demand resume feature reduces the time until the VM resumes execution drastically: from ten seconds and more to 2 seconds.
Further, with on-demand resume the resume speed is independent of the total memory dump size,
but only depends on the actual working set size of the VM.
If, for example, only a few memory pages are required by the VM to answer an HTTP request,
most of the dump is never read from disk.

To summarize: resuming a virtual machine currently takes on the order of tens of seconds.
There is a fixed overhead which depends on the VM's configured memory size.
Besides the fixed overhead, there is also a variable overhead which increases with the virtual machine's dump size.
The dump size naturally grows over time as the VM uses more memory.
To resume execution, however, it is unnecessary to read the VM's memory entirely from disk.
We propose to cut the resume time by only reading the required pages from disk and only when they are needed.
Doing so, the resume time can be reduced from tens of seconds to less than three seconds.

%% \subsection{Motivating Example}

%% \note{TK: Figure about our web service and how much time/money can be saved with on-demand services.}

%% \pagebreak
%% \newpage

%% Virtual machine suspend and resume
%% allow to start and stop the execution of a virtual machine at
%% arbitrary points in time. A suspended virtual machine consumes neither
%% CPU nor memory.

%% Improving virtual machine resume time is important because it enables
%% new use cases. Currently, the resume time is dominated by the time it
%% takes to read the memory content from disk into memory. For example, a
%% virtual machine with 4GB of memory resumes in approximately 40
%% seconds.

%% Figure: breakdown of resume time

%% \cite{zhu2011twinkle}

%% \section{Architecture}

%% Call me maybe.

\section{Qemu Primer}

We focus our effort on the most recent version 1.4 of the open-source virtualization system \emph{qemu}.
qemu is a cross-platform, full system emulator which supports various target architectures, for example, ARM and X86.
We concentrate exclusively on the Linux version of qemu.
On Linux, there exists an accompanying kernel module, kvm, which significantly increases the guest performance.
%% Instead of emulating each and every instructions, with kvm, they can be directly executed on the hardware.
%% Only privileged instructions must be intercepted and emulated.
The kvm kernel module provides an interface to the host OS kernel.
qemu uses this interface to speed up the emulation process by performing certain operation directly inside the kernel.
There is, however, no fundamental difference in the functionality provided with or without the kvm kernel module.

Before we discuss how to improve the virtual machine resume time,
we describe how the suspend/resume functionality is implemented in qemu.

\subsection{qemu suspend/resume}

The implementation of VM suspend/resume is conceptually similar to offline migration.
Consequently, there is significant overlap in the code to migrate and suspend/resume VMs.
Each operation involves the following steps:

\begin{enumerate}
\item Halt the execution. The VM no longer executes any instructions. As a consequence, the VM's state does not change anymore.
This is the main difference between offline and live migration.
For a discussion of live migration algorithms see, for example, \cite{clark2005livemigration, hines2009post}.
\item Capture the VM's state by writing it to a persistent storage location, e.g., disk.
The state consists of the guest's memory as well as the guest CPU's registers and devices.
The guest's memory accounts for the majority of the state.
For example, in our evaluation, the CPU and device state comprises less than one megabyte,
while the guest's memory configuration is typically in the gigabyte range.
\item Optional: transfer the persisted state to the migration destination.
This is only necessary for (offline) migration and is done outside of qemu,
i.e., traditional system administrator tools, like \verb+scp+, can be used to transfer the persisted state.
\item Re-instantiate a virtual machine based on the persisted state.
A command line parameter indicates to qemu that instead of bootstrapping the VM from scratch,
the previously persisted state is used.
In-memory structures are created and populated with data read from disk.
\item Start executing instructions again.
The VM is now ready to service requests.
\end{enumerate}

While it is possible to re-purpose the suspend/resume functionality to perform offline migration,
``true'' offline migration does not persist the guest's state anywhere.
Instead, the guest's state is copied from the migration source to the destination only ever touching memory.
Now that we have introduced the basic suspend/resume steps,
we detail the suspend file format.

%% The VM's current execution context must be captured and saved.
%% The execution context consists of the CPU state, i.e., register content,
%% device state, e.g., framebuffer content,
%% and main memory.
%% The bulk of the execution context consists of main memory.
%% Register and device state is less than one megabyte for a typical VM.
%% When optimizing the resume time of a virtual machine, it makes sense to concentrate on the RAM state.

\subsection{State transfer protocol/file format}

Understanding the file format is important in order to optimize the resume process.
Every time a VM is resumed, qemu reads through the suspend image from start to end.
The file starts with a magic number (\verb+QEMU_VM_FILE_MAGIC+) for basic input validation.
This is standard practice to prevent accidentally reading a file with an incomprehensible format.
Next comes a version number (\verb+QEMU_VM_FILE_VERSION+).
Version numbers are used to detect compatibility problems.
Features existing in older version of qemu may have been deleted,
which may prevent a new qemu version to read a suspend image written with an older version.

%% \begin{lstlisting}[caption=savevm.c,
%% label=code:qemu_savevm_state,
%% captionpos=b]
%% qemu_savevm_state(QEMUFile* f) {

%%   qemu_savevm_state_begin(f, &params);
%%   while(...) {
%%     qemu_savevm_state_iterate(f);
%%   }
%%   qemu_savevm_state_complete(f);
%% }

%% qemu_savevm_state_begin(...) {
%%   qemu_put_byte(f, QEMU_VM_SECTION_START);
%% }

%% qemu_savevm_state_iterate(...) {
%%   qemu_put_byte(f, QEMU_VM_SECTION_PART);
%% }

%% qemu_savevm_state_complete(...) {
%%   qemu_put_byte(f, QEMU_VM_SECTION_END);
%%   save_live_complete(...);
%%   qemu_put_byte(f, QEMU_VM_SECTION_FULL);
%%   save_state(...);
%%   qemu_put_byte(f, QEMU_VM_EOF);
%% }

%% \end{lstlisting}

%% While the actual implementation is invoked through a file pointer.

After the basic validation and compatibility checks are performed,
the remainder of the file is divided into sections with one section per device or subsystem.
Standard subsystems are, for example, interrupt controller, serial port, or DMA controller.
Because the implementation details for each section differ,
the variable \verb+savevm_handlers+ contains information about how to handle each subsystem.

By far the largest section is dedicated to main memory.
The main memory section is itself divided into multiple sub-sections,
each containing the information for a part of the entire address space.
Each sub-section is a succession of page-by-page information.
The core logic is in the function \verb+ram_save_block+ (file \verb+arch_init.c+).
Pages are either stored uncompressed in four kilobyte (one page) chunks,
or compressed.
The compression scheme is simple yet effective:
if a 4 kilobyte block only contains a single byte pattern,
only one byte is stored for this page.
Compression is sensible because address space utilization is typically sparse,
with unused address space regions containing only zeros.
On the other hand, zero pages will gradually disappear while the VM runs.
Applications allocate and deallocate memory as part of their normal mode of operation.
When applications release memory, it is usually not overwritten with zeroes again.
Hence, over time, the potential to compress ``empty'' pages decreases.

%% Various bits of the 64 bit integer are used to store additional flags.
%% The flags, for example, indicate whether a block is compressed.
%% Each address is followed by a block of data.
%% The block's size is identical to the target page size, e.g., 4~kilobytes.
%% The last address/block pair has an end-of-section flag set in the address.

For the purpose of optimizing the VM resume time it is important to note that the suspend file is not a one-to-one mapping of the guest's memory content.
That is, byte N in the suspend image does not correspond to byte N in the guest's address space. 
Compression and additional meta-information destroy the one-to-one mapping.
Even a simple offset is insufficient to establish a direct mapping.
While it makes sense to employ compression to save disk and network bandwidth when migrating VMs,
it complicates resuming an instance.
This is why we decided to change the suspend image format.

%% Typically, most of the VM's memory will not be utilized and either contain garbage or zeros.
%% To save disk space and reduce suspend time qemu employs a simple form of compression.
%% Whenever a single page, i.e., 4k of data, consist of the same byte pattern,
%% only the byte pattern is written to the suspend file.
%% Instead of writing 4k, only a single byte value is stored.
%% This is, for example, why suspending a freshly started VM instance with 4~GB of memory only creates a suspend image of roughly 400~MB.
%% However, for the purpose of quickly resuming a suspended VM a different layout is more beneficial.

%% \note{TK: zero pages disappear gradually as time goes by.
%% application allocate/deallocate memory, use it, and not explicitly fill it with zeros again.
%% improving performance with increasing suspend images thus is important.}

%% \begin{lstlisting}[caption=libvirt suspend image header,
%% label=code:libvirt-header,
%% captionpos=b]
%% <domain type='kvm'>
%%   <name>192.168.2.131</name>
%%   <uuid>671512e6-9d25</uuid>
%%   <memory>2097152</memory>
%%   <currentMemory>2097152</currentMemory>
%%   <vcpu>1</vcpu>
%%   <os>
%%     <type arch='x86_64' 
%%           machine='pc-1.0'>hvm</type>
%%     <boot dev='hd'/>
%%   </os>
%%   ...
%% </domain>
%% \end{lstlisting}

\subsection{Interoperability}

Note that software libraries, such as libvirt,
which provide a hypervisor-agnostic way of managing virtual machines,
wrap the original suspend file.
The wrapper contains additional meta-data specific to the library.
For example, libvirt adds information about the VM's configuration data to the suspend file.
An important design aspect when modifying the suspend image format is to preserve interoperability with existing software tools.
Our modifications to qemu's file format still allow us to manage VMs with libvirt.
%% Our qemu modifications are invisible to libvirt.
%% Users of our patched qemu are still able to use libvirt as usual.

\section{Implementation}
\label{sec:implementation}

In this section we detail the optimizations we performed to shorten the time to resume a suspend VM.

\subsection{Gratuitous memory zeroing}

We first tackle the resume overhead which depends on the configured memory size.
As we have shown in Figure~\ref{fig:resume-times-unoptimized},
the resume time not only depends on the suspend image's size,
but also on the configured memory size.
That is, for identical resume image sizes it takes longer to resume the more configured memory the instance has.
Clearly, there must be an overhead which is proportional to the configured memory size.
Intuitively, resuming an instance with 4~GB of configured memory should be as fast as resuming an instance with 1~GB of memory,
if their on-disk state has the same size.

The answer lies in the way qemu stores resume images:
as explained earlier, qemu employs memory compression to reduce the resume image's size.
Instead of explicitly storing pages where all bytes are identical,
qemu only stores the byte pattern once.
A flag in the page's header indicates whether the page is compressed.
When resuming the instance, qemu restores the compressed page by calling \verb+memset()+ with the appropriate byte pattern.
Listing~\ref{code:memset-unoptimized} shows the corresponding code.

For instances with differing memory configurations but identical suspend images sizes this means that the image belonging to the instance with more memory contains many more (compressed) pages.
Even a simple operation as zeroing a block of memory via \verb+memset()+ adds to the runtime cost
if it is performed thousands of times.
For example, zeroing one gigabyte of memory 4 kilobyte at a time invokes \verb+memset()+ $2^{18}$ times.
It also adds about one second (on our systems) per gigabyte to the resume time.

%% As a result of this compression technique,
%% the actual image size only grows insignificantly when increasing the configured memory size.
%% Table~\ref{tab:minimal-suspend-image} contains examplary suspend image sizes.
%% The data was collected using the following procedure:
%% we booted a fresh Ubuntu 12.04 guest until it reached the login prompt.
%% As soon as the login prompt appeared,
%% we issued a suspend-to-disk and recorded the suspend image's file size.

%% % ls -lha /tmp/dump-?g 
%% -rw-r--r-- 1 thomas thomas 166M May 24 14:18 /tmp/dump-1g
%% -rw-r--r-- 1 thomas thomas 196M May 24 14:20 /tmp/dump-2g
%% -rw-r--r-- 1 thomas thomas 316M May 24 14:22 /tmp/dump-4g
%% -rw-r--r-- 1 thomas thomas 422M May 24 14:24 /tmp/dump-8g

%% \begin{table}
%%   \begin{center}
%%     \begin{tabular}{r|r|r|r|r}
%%       configured memory size & 1~GB & 2~GB & 4~GB & 8~GB\\
%%       \hline
%%       suspend image size & 166~MB & 196~MB & 316~MB & 422~MB\\
%%     \end{tabular}
%%   \end{center}
%%   \caption{.}
%%   \label{tab:minimal-suspend-image}
%% \end{table}

\begin{lstlisting}[caption=VM resume with gratuitous memory zeroing (cf. qemu/arch\_init.c\, ram\_load()),
label=code:memset-unoptimized,
captionpos=b]
ch = qemu_get_byte(f);
memset(host, ch, TARGET_PAGE_SIZE);
\end{lstlisting}

Looking further into this issue we found that the most common byte pattern for compressed pages was, in fact, zero.
% When resuming a virtual machine, however, the memory allocation routine already initializes the memory to zero.
However, explicitly zeroing memory is, strictly speaking, unnecessary.
In Linux, for example, the system call \verb+mmap+,
used for allocating memory,
guarantees zero-filled pages for anonymous mappings.
\verb+mmap+ does this by pointing the page table entry for every address to a special ``zero page''.
This works as long as there are only read accesses.
When there is a write access, a new page frame is allocated and the page's content cleared.
Before the write access can proceed,
the page table entry for the address is updated by pointing to the newly allocated and zeroed page.
The advantage is that only pages accessed for writing are zeroed and not when resuming the instance,
but at the time of the write access.
%% http://lxr.free-electrons.com/ident?i=do_anonymous_page has the details
We approximate this behavior by only calling \verb+memset()+ if the byte pattern is non-zero.
Listing~\ref{code:memset-optimized} illustrates the improved version.

\begin{lstlisting}[caption=disabled gratuitous memory zeroing,
label=code:memset-optimized,
captionpos=b]
ch = qemu_get_byte(f);
if (unlikely(ch != 0)) {
    memset(host, ch, TARGET_PAGE_SIZE);
}
\end{lstlisting}

This optimization is particularly effective for short-lived or significantly over-provisioned instances.
Those instances contain a large number of zero pages,
because the memory was never utilized.
For long running instances the fraction of zero pages may be increased by well-known techniques such as memory ballooning~\cite{waldspurger2002memory}.
The balloon driver, upon allocating memory inside the VM, can reset it to zero~\cite{hines2009post}.
A subsequent suspend then finds signigicantly more compressible zero pages than before the ballooning operation.
However, we did not evaluate our optimization in combination with memory ballooning and leave it for future work.

\subsection{On-demand memory restore}

The previous optimization, while helpful, still requires to read the entire suspend image from disk. 
Depending on the suspend image's size, the resume takes anywhere from 2 seconds up to 10 seconds or more.
In order to decrease the resume time, especially for instance with suspend images of multiple gigabytes,
even further, we propose to only selectively read the data from disk.
To do useful work, only the VM's working set must be in memory to continue execution.

With the existing suspend image format this is not straightforward.
We would need a mechanism to map memory accesses inside the virtual machine to file accesses in the host OS.
This is possible, for example by writing a custom device driver, which runs as part of the host OS,
and accessing the driver from within the qemu process.
However, an easier solution exists, if we change the format of the suspend image.

By disabling memory compression and writing the entire memory content from start to finish,
we have a file with the same size as the guest's memory.
We then use the \verb+mmap()+ system call to map the file's content into qemu's address space.
Whenever the qemu process, i.e., the virtual machine, accesses a memory location within the \verb+mmap()+ed region,
the operating system's page fault handler reads the appropriate 4 kilobyte block from disk.
With minimal changes to the qemu code,
we have implemented our on-demand resume functionality.

Writing an uncompressed memory image to disk sounds like a bad idea as it increases the write traffic.
This, however, must not be the case.
We can employ the same trick of ``skipping'' zero pages that the original implementation used.
Modern file systems, such as ext4, support \emph{sparse files}.
A sparse file only occupies disk space proportional to the size of its non-zero content.
By allocating a sparse file and only writing at offsets which contain non-zero pages,
we reduce the amount of data written to disk to a minimum.
Actually, we write less data to disk than before because no meta-data, e.g., page offsets, must be stored.

Another implication of using \verb+mmap()+ed memory as the guest's RAM is that suspending a resumed instance only writes disk blocks which have changed since the VM was resumed.
The host operating system tracks page updates for the mapped region.
Only the changed pages must be synchronized back to disk when the instance is suspended subsequently.
Previously, for each suspend the guest's entire memory was written to disk,
albeit in a compressed format if possible.

The Linux \verb+mmap()+ system call distinguishes between two types of mappings: shared and private.
As the name suggests, a shared mapping can potentially be used to exchange information between multiple threads.
That is, updates to the mapped region are visible in other threads mapping the same file.
As a side-effect, the updates are also applied to the on-disk file.
While the in-memory updates are visible to other threads immediately,
the point in time at which the underlying file is updates is configurable,
with a default of 30 seconds.
The updates may only be written to disk when the region is unmapped again.
Alternatively, updates can be forced to disk with the system call \verb+msync()+.

%% TK: Have to be careful here. Not sure how multi-core VMs may react to a private mmap region.
%% For our implementation we are not interested in sharing mapped region between multiple threads/processes.
Besides the shared memory semantics, what is of more immediate interest to us,
is the update semantic of the underlying file.
Ideally, we would like to have something between the currently available options.
We want to persist in-memory updates before exiting the process, i.e., when we suspend the virtual machine.
This behavior is consistent with the semantics of a shared mapping.
Intermediate and repeated updates to the same page, however,
should not be written to disk as this is unnecessary write traffic.
Unfortunately, the existing interface does not allow to specify such behavior~\footnote{The code can be found, for example, at \url{http://lxr.linux.no/linux+v3.2.5/fs/fs-writeback.c}.}

Considering our options with the current \verb+mmap()+ semantics we decided to use a shared mapping.
Persisting the guest's memory is a simple as a single \verb+munmap()+ call.
In future work we plan to address the issue of not writing intermediate memory updates to disk.

%% \note{TK: interaction between sparse file and mmap()?}

%% \note{Discuss MAP\_PRIVATE vs. MAP\_SHARED}

%% \note{Subsequent suspends should be almost instantaneous!}

%% \begin{lstlisting}[caption=On-demand VM resume using the OS's on-demand paging mechanism. We modified the function qemu\_ram\_alloc\_from\_ptr in the file exec.c.,
%% label=code:on-demand-resume,
%% captionpos=b]
%% if (0 == strncmp(mr->name, ``pc.ram'',
%%                  strlen(``pc.ram'')) &&
%%     0 == stat(``/mnt/thomas/ram'', &sb) &&
%%     (S_ISREG(sb.st_mode) || S_ISBLK(sb.st_mode))) {

%%     new_block->fd_ram = open(``/mnt/thomas/ram'', O_RDWR);
%%     if (new_block->fd_ram < 0) {
%%         DPRINTF(``errno: %s\n'', strerror(errno));
%%     }
%%     assert(new_block->fd_ram >= 0);
%%     new_block->host = mmap(NULL, size, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_PRIVATE,
%%                            new_block->fd_ram, 0);
%% }
%% \end{lstlisting}

%% \subsection{Link time optimizations}

%% New version of gcc support link time optimizations, e.g., inlining across object file boundaries.
