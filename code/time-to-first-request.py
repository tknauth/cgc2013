#!/usr/bin/env python2.7

import pdb
import time
import subprocess as sp
import shlex
import sys
import numpy as np
import itertools
import os
import multiprocessing

base_ip = 160

def wait_for_http_server(ip) :
    count = 0
    while 0 != sp.call(shlex.split("curl --fail http://%s/"%(ip)), stdout=None) :
        count += 1
        time.sleep(1)
        if count >= 20 :
            print 'failing ... web service did not come up'
            sys.exit(1)

# @return Delay (in seconds) between issuing virsh restore command and receiving HTTP reply.
def resume_first_request_latency() :

    drop_caches()

    begin = time.time()
    rc = sp.call(shlex.split("virsh restore /mnt/%s-dump"%(virsh_name)))
    if rc != 0 :
         sys.exit(1)

    while True :
        rc = sp.call(shlex.split("curl --fail http://%s/"%(virsh_name)), stdout=None)
        if rc == 0 :
            break
    end = time.time()

    rc = sp.call(shlex.split("virsh save %s /mnt/%s-dump"%(virsh_name, virsh_name)))
    if 0 == rc :
        time.sleep(3)
    else :
        assert False

    return end - begin

def wait_till_shutdown(virsh_name) :
    while True :
        rc = sp.call("virsh list | grep %s"%(virsh_name),
                     stdout=None, stderr=None, shell=True)
        if rc != 0 :
            break
        time.sleep(1)

def run(v, virsh_name, time_to_response) :

    begin = time.time()
    if v['action'] == 'start' :
        p = sp.Popen(shlex.split("virsh start %s"%(virsh_name)))
    elif v['action'] == 'resume' :
        p = sp.Popen(shlex.split("virsh restore /mnt/%s-dump"%(virsh_name)))
        
    while True :
        rc = sp.call(shlex.split("curl --fail http://%s/"%(virsh_name)), stdout=None)
        if rc == 0 :
            break
    end = time.time()

    time_to_response.value = (end-begin)

def setup_disk(v) :
    if v['disk'] == 'hdd' :
        sp.check_call(shlex.split("sudo mount /dev/disk/by-id/scsi-SATA_SAMSUNG_HD204UIS2H7J1CB117088-part4 /mnt"))
    elif v['disk'] == 'ssd' :
        sp.check_call(shlex.split("sudo mount /dev/disk/by-id/scsi-SATA_INTEL_SSDSC2CT1CVMP244301DC120BGN-part1 /mnt"))

def teardown_disk(v) :
    if v['disk'] == 'ssd' or v['disk'] == 'hdd' :
        sp.check_call(shlex.split("sudo umount /mnt"))
    else :
        assert False

def setup_qemu(v) :
    if v['qemu'] == 'stock' :
        sp.check_call(shlex.split("rm /home/thomas/bin/kvm"))
        sp.check_call(shlex.split("ln -s /home/thomas/bin/qemu-system-x86_64v1_4 /home/thomas/bin/kvm"))
    elif v['qemu'] == 'custom' :
        sp.check_call(shlex.split("rm /home/thomas/bin/kvm"))
        sp.check_call(shlex.split("ln -s /home/thomas/bin/qemu-system-x86_64 /home/thomas/bin/kvm"))

def teardown_qemu(v) :
    if v['qemu'] == 'custom' :
        assert 0 == sp.call(shlex.split("rm /home/thomas/bin/kvm"))
        assert 0 == sp.call(shlex.split("ln -s /home/thomas/bin/qemu-system-x86_64 /home/thomas/bin/kvm"))
        
    elif v['qemu'] == 'stock' :
        pass

def setup_cache(v) :
    if v['cache'] == 'hot' :
        pass
    elif v['cache'] == 'cold' :
        sp.check_call(shlex.split("sudo sync"))
        sp.check_call("echo 1 | sudo tee /proc/sys/vm/drop_caches", shell=True)

def setup_action(v) :
    for i in range(v['concurrent']) :
        virsh_name = '192.168.2.%d'%(base_ip+i)
        if v['action'] == 'resume' :
             sp.check_call(shlex.split("virsh start %s"%(virsh_name)))
             wait_for_http_server(virsh_name)
             sp.check_call(shlex.split('curl http://%s/ram/%d'%(virsh_name, v['ramuse_mb'])))
             sp.check_call(shlex.split("virsh save %s /mnt/%s-dump"%(virsh_name, virsh_name)))
        elif v['action'] == 'start' :
            pass

def teardown_action(v) :
    for i in range(v['concurrent']) :
        virsh_name = '192.168.2.%d'%(base_ip+i)
        if v['action'] == 'resume' :
            sp.check_call(shlex.split("virsh shutdown %s"%(virsh_name)))
            wait_till_shutdown(virsh_name)
            sp.check_call(shlex.split("rm --force /mnt/%s-suspend-image"%(virsh_name)))
            sp.check_call(shlex.split("rm --force /mnt/%s-dump"%(virsh_name)))
        elif v['action'] == 'start' :
            sp.check_call(shlex.split("virsh shutdown %s"%(virsh_name)))
            wait_till_shutdown(virsh_name)

def teardown_cache(v) :
    pass

def setup_ramcfg_mb(v) :
    for i in range(v['concurrent']) :
        virsh_name = '192.168.2.%d'%(base_ip+i)
        sp.check_call('virsh dumpxml %s > /tmp/%s.xml'%(virsh_name, virsh_name), shell=True)
        sp.check_call(shlex.split('sed -i "s/<memory>.*<\/memory>/<memory>%d<\/memory>/" /tmp/%s.xml '%(v['ramcfg_mb']*1024, virsh_name)))
        sp.check_call(shlex.split('sed -i "s/<currentMemory>.*<\/currentMemory>/<currentMemory>%d<\/currentMemory>/" /tmp/%s.xml '%(v['ramcfg_mb']*1024, virsh_name)))
        if v['qemu'] == 'stock' :
            pass
        elif v['qemu'] == 'custom' :
            from xml.dom.minidom import parse
            dom = parse('/tmp/%s.xml'%(virsh_name))
            if not dom.getElementsByTagName('qemu:commandline') :
                e = dom.createElement('qemu:commandline')
                e.appendChild(dom.createElement('qemu:arg'))
                e.childNodes[0].setAttribute('value', '-suspend-image')
                e.appendChild(dom.createElement('qemu:arg'))
                e.childNodes[1].setAttribute('value', '/mnt/%s-suspend-image'%(virsh_name))
                assert len(dom.childNodes) == 1
                dom.childNodes[0].appendChild(e)
                dom.childNodes[0].setAttribute('xmlns:qemu', 'http://libvirt.org/schemas/domain/qemu/1.0')
                dom.getElementsByTagName('emulator')[0].childNodes[0].replaceWholeText('/home/thomas/bin/kvm')
                with open('/tmp/%s.xml'%(virsh_name), 'w') as f :
                    f.write(dom.toxml())

                # print dom.toxml()
        sp.check_call(shlex.split('virsh define /tmp/%s.xml'%(virsh_name)))

def teardown_ramcfg_mb(v) :
    for i in range(v['concurrent']) :
        virsh_name = '192.168.2.%d'%(base_ip+i)
        sp.check_call('virsh dumpxml %s > /tmp/%s.xml'%(virsh_name, virsh_name), shell=True)
        sp.check_call(shlex.split('sed -i "s/<memory>.*<\/memory>/<memory>%d<\/memory>/" /tmp/%s.xml '%(1024, virsh_name)))
        sp.check_call(shlex.split('sed -i "s/<currentMemory>.*<\/currentMemory>/<currentMemory>%d<\/currentMemory>/" /tmp/%s.xml '%(1024, virsh_name)))

        if v['qemu'] == 'custom' :
            from xml.dom.minidom import parse
            dom = parse('/tmp/%s.xml'%(virsh_name))
            e = dom.getElementsByTagName('qemu:commandline')
            assert len(e) == 1
            e[0].parentNode.removeChild(e[0])
            with open('/tmp/%s.xml'%(virsh_name), 'w') as f :
                f.write(dom.toxml())

        sp.check_call(shlex.split('virsh define /tmp/%s.xml'%(virsh_name)))

def get_qemu_maj_page_faults(v) :
    maj_faults = []
    for i in range(v['concurrent']) :
        virsh_name = '192.168.2.%d'%(base_ip+i)
        pid = sp.check_output(shlex.split('pgrep -f %s'%(virsh_name))).strip()
        v = sp.check_output(shlex.split('ps -o maj_flt= %s'%(pid))).strip()
        maj_faults.append(int(v))

    return maj_faults

def run_experiment(v, measurements) :
    setup_ramcfg_mb(v)
    setup_disk(v)
    setup_qemu(v)
    setup_action(v)
    setup_cache(v)

    time_to_respond = []
    ps = []
    for i in range(v['concurrent']) :
        time_to_respond.append(multiprocessing.Value('d', 0.0))
        virsh_name = '192.168.2.%d'%(base_ip+i)
        p = multiprocessing.Process(target=run, args=(v, virsh_name, time_to_respond[i]))
        p.start()
        ps.append(p)

    for p in ps : p.join()

    measurements['dumpsize_byte'] = 0
    virsh_name = '192.168.2.160'
    if v['action'] == 'resume' :
        measurements['dumpsize_byte'] = os.stat('/mnt/%s-dump'%(virsh_name)).st_size

    measurements['maj_pg_faults'] = get_qemu_maj_page_faults(v)

    teardown_cache(v)
    teardown_action(v)
    teardown_qemu(v)
    teardown_disk(v)
    teardown_ramcfg_mb(v)

    return [x.value for x in time_to_respond]

if __name__ == '__main__' :
    vars = {'disk' : ['hdd', 'ssd'],
            'qemu' : ['stock', 'custom'],
            'action' : ['resume'],
            'cache' : ['cold'],
            'ramcfg_mb' : [1024*i for i in [2]],
            'ramuse_mb' : [1] +[512*i for i in range(1,4)],
            'concurrent' : [1,4]}

    key_order = ['disk', 'qemu', 'action', 'cache', 'ramcfg_mb', 'ramuse_mb', 'concurrent']

    timestamp = int(time.time())
    f = open("%d.csv"%(timestamp), 'w')
    for (k, v) in vars.iteritems() :
        print >> f, k, 
    print >> f, 'time_s', 'dumpsize_byte', 'maj_pg_faults'

    for tuple in itertools.product(*[vars[k] for k in key_order]) :
        parameters = {}
        for i in range(len(tuple)) : parameters[key_order[i]] = tuple[i]

        # skip illegal parameter configurations
        if parameters['ramuse_mb'] > (parameters['ramcfg_mb'] - 512) : continue

        for (k,v) in parameters.iteritems() :
            print >> f, v,

        for n in range(1) :
            measurements = {}
            took = run_experiment(parameters, measurements)
            print >> f, took, measurements['dumpsize_byte'], measurements['maj_pg_faults']
            f.flush()

    f.close()
