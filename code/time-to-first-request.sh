#!/bin/bash

python2.7 -c 'import time ; print time.time()'
virsh restore /mnt/thomas/192.168.2.164-dump
while [[ true ]] ; do
  curl --fail http://192.168.2.164/
  if [ $? == 0 ] ; then
    break;
  fi
done
python2.7 -c 'import time ; print time.time()'
