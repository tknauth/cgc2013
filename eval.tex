\section{Evaluation}

We modified the most recent release of qemu to evaluate our optimizations.
The measurements were collected on a machine equipped with a 4-core AMD Phenom II processor, 12~GB of memory, and an SSD from Intel (model SSDSC2CT12).
We used Ubuntu 12.04.1 as our operating system with a 3.2.0-32 kernel.
This machine hosts our VM instances.

The virtual machines were created with Ubuntu's vmbuilder tool~\footnote{\url{https://help.ubuntu.com/12.04/serverguide/jeos-and-vmbuilder.html}}.
vmbuilder allows the user to easily create virtual machine images from scratch.
The services and drivers installed as part of the operating system are specifically tailored to a virtualized environment.
In addition to the standard packet selection, we installed an ssh and web server.
%% Don't mention second disk here: VM disk is in page cache anyhow.
%% The virtual machine images are stored on the spinning disk.

Besides our virtual machine host we have a second machine to issue HTTP requests.
The two machines are connected via a conventional Gigabit switch.
For experiments where we measure the resume time,
we measure how much time passes between issuing the resume request until the first successful HTTP response is received.

Besides the initial delay until the first request is answered,
we are also interested in the runtime performance impacts of our on-demand resume functionality.
We use the RUBiS~\cite{rubis} benchmark suite to evaluate this.
To gauge the impact we record the request latencies over time.
While we measure the end-to-end latency for the initial request delay,
we here measure the request processing latency.
First, the data is easier to collect, as RUBiS's standard output does not contain per-request latency data but only average values.
Second, end-to-end data is not necessary, because our on-demand resume only affects the server-side processing of requests.
Everything else, e.g., the network, is unaffected.
Hence, measuring the request processing latency within the VM still provides meaningful data.

%% \note{TK: redo Figure 2. Must measure time till received HTTP response.}

%% \note{SSDs are a good fit because read/write cycles are explicitly controlled at application level.
%% Cite something which highlights the challenges working with SSDs.}

\begin{figure}
      \includegraphics{figs/1364912351-resume}
      \caption{Improved resume times by disabling gratuitous page zeroing.}
      \label{fig:resume-time-no-zero-pages}
\end{figure}

\begin{figure*}
  \begin{tabular}{cc}
    \begin{minipage}[t]{\columnwidth}
      \includegraphics{figs/time-to-first-answer-4096}
      \caption{VM configured with 4~GB of memory.
Resume time increases linearly for the default resume implementation.
It stays constant at 3 seconds with our lazy resume implementation.}
      \label{fig:time-to-first-answer-4096}
    \end{minipage}
    &
    \begin{minipage}[t]{\columnwidth}
      \includegraphics{figs/1375887435-scalability-bar}
      \caption{Resuming four instance in parallel.
        Each instance was configured with 2~GB of memory.
We show results for varying amounts of \emph{used} memory.
With the default implementation, the resume time increases linearly with used memory.
Our customization keeps the resume time constant and the benefits increase with larger suspend image sizes.}
      \label{fig:scalability}
    \end{minipage}
  \end{tabular}
\end{figure*}

\subsection{Disabling gratuitous memory zeroing}

In Section~\ref{sec:implementation} we detailed two possible modifications to improve the resume latency.
Our first idea does not change the suspend image format, while the second optimization is more intrusive.
We have collected empirical data on the effect of each optimization:

Figure~\ref{fig:resume-time-no-zero-pages} shows the impact of disabling gratuitous page zeroing on the resume time.
The baseline resume latency was shown in Figure~\ref{fig:resume-times-unoptimized} which we have to compare to.
Looking at Figure~\ref{fig:resume-time-no-zero-pages} we conclude that our optimization successfully removes the configuration dependent resume penalty.
Regardless of the VM's configured memory size,
it takes less than 2 seconds to resume a virtual machine from a 400~MB suspend image.
That is, baseline resume time for instances with 4~GB of memory was reduced by 3 seconds, from 5 seconds down to 2.
Similarly, instances with 2~GB of memory now also resume in 2 seconds, compared to 3 seconds previously.
While this is an improvement, it still takes more than 10 seconds to resume images of 4000~MB.

%% \note{measure min/max/average response times to gauge impact on-demand paging; use RUBiS benchmark}

\subsection{On-demand resume}

%% For the measurements reported here,
%% we stored the suspend images on a single SSD.
%% This explains why the absolute resume times are higher than the previous results.
%% Nonetheless, the trends we discuss here also apply to settings with a higher I/O bandwidth,
%% i.e., resume times are independent of the suspend image's size. 

If we enable our on-demand resume feature,
we can successfully resume even instances with large memory images, e.g., 4~GB, in about 2 seconds.
Figure~\ref{fig:time-to-first-answer-4096} shows the resume latency for virtual machines configured with 4~GB of RAM.
We are taking advantage of the fact that only a fraction of the entire 4~GB suspend image is required to service requests.
The curve for our modified qemu stays flat across the whole range of suspend image sizes.
For large suspend images, the resume time is reduced to 1/15th, i.e.,
it takes the unmodified qemu 36.5 seconds to resume the instance,
compared to 2.4 seconds for our customized resume implementation.

To determine the lazy resume overhead for answering the first HTTP request successfully,
we recorded the number of major page faults for the qemu process.
Major page faults occur whenever the page fault handler must read data from disk to populate a particular page.
To determine the worst-case scenario,
we emptied the page cache before each experiment.
Otherwise, the accessing an address in the mapped region may only cause a minor page fault because the data may still reside in the cache.

We found that, on average, between 700 to 900 major page faults occurred until the first successfully answered HTTP request.
Because disk reads are expensive, more than one page at a time is read from disk.
Hence, it is not straightforward to tell how much data is actually read from disk by just looking at the major page fault counter.
We can, however, utilize other ways to find out how much of the memory mapped region is actually present in memory.
By reading the virtual file /proc/$<$pid$>$/smaps of the qemu process, we determined that about 7-8~MB of the backing file were resident in the host's memory.
Depending on the suspend image size,
this is \emph{two to three orders of magnitude} less data which is read from memory compared to the standard resume procedure.
For a moderately sized suspend image of 1~GB, less than 1\% of the image is required to successfully respond to an HTTP request.

With the prevalence of multi-core CPUs the number of virtual machines a single server can support is steadily increasing.
More instances per server also means that more than a single instance may be resumed at once.
Resuming multiple instances simultaneously is even more demanding,
because more state must be transferred from disk to memory.
We tested the scalability of lazily resuming instances by resurrecting four instances in parallel.
The results are shown in Figure~\ref{fig:scalability}.
All instances were configured with 2~GB of memory and we varied the amount of \emph{used} memory between runs.
The time to resume the instances increases linearly with the used memory for the unmodified qemu implementation.
With a suspend image size of 1800~MB it takes on average 43 seconds to resume each instance.
By lazily resuming the instances the linear relationship to the suspend image size is cut.
It takes 6-7 seconds, regardless of the suspend image size, to lazily resume four instances in parallel.
Resuming four instances in parallel increases the resume time by 2x for lazy resume,
i.e., 6 seconds with 4 instances vs. 3 seconds for a single instance.
For suspend images sized 1800~MB, the time to resume for eager resume increases by 4x:
44 seconds for four instances compared to 12 seconds for a single instance.

\subsection{Impact on continued execution}

\begin{figure*}
  \begin{tabular}{cc}
    \begin{minipage}[t]{\columnwidth}
        \includegraphics{figs/latency-over-time}
      \caption{Time series plot of the request processing latency.
The majority of requests takes less than 1~millisecond to process.
On-demand resume has no noticeable impact on the processing latency.}
      \label{fig:latency-over-time}
    \end{minipage}
    &
    \begin{minipage}[t]{\columnwidth}
      \includegraphics{figs/latency-cdf}
      \caption{Comparing the latency distribution for a VM started from scratch and an on-demand resumed VM.
The two curves are virtually indistinguishable, i.e., the on-demand paging does not negatively impact the request processing latency.}
      \label{fig:latency-cdf}
    \end{minipage}
  \end{tabular}
\end{figure*}

Responding quickly to the first request is important.
But we also have to evaluate the impact of faulting in memory pages on-demand over longer periods of time.
While the virtual machine executes and answers more requests,
more and more pages must be transferred from disk to memory.
We used the RUBiS benchmark to subject the VM to 15 minutes of continued requests and compare the request processing times in Figure~\ref{fig:latency-over-time} and Figure~\ref{fig:latency-cdf}.
The former shows a time series plot of the processing latency,
while the latter shows the same data in the form of a cumulative distribution function~(CDF).

The time series plot reveals that most requests are processed in less than 500 microseconds,
regardless of whether the VM was resumed or started from scratch.
Maximum processing time is less than 1.5 milliseconds, also in either case.
Intuitively, from the time series data we conclude that on-demand resume has no noticeable impact on the request processing time.
This is indeed confirmed by looking at the CDF: the distribution for either case, on-demand resume and start, are very similar, indeed barely distinguishable.
Hence, we conclude that on-demand resume is effective at reducing the resume time to about 3 seconds.
At the same time on-demand resume has no discernable impact on the continued execution performance of the VM.

% \note{TK: have graph with page faults \# over time.}

%% We first evaluated the time it takes from issuing the resume command until the first HTTP request is answered.
%% The delay is important for two reasons:
%% for web services with only a single instance the delay directly adds to the user-perceived latency.
%% For web services with multiple instances, the resume time translates into the ability to scale out processing power quickly.
%% Figure~\ref{fig:time-to-first-answer-2048} and \ref{fig:time-to-first-answer-4096} show first request latency, for virtual machines configures with 2 and 4~GB of RAM, respectively.
%% We show two plots for the different configurations, because times differ for the stock qemu based on memory configuration.

% \subsection{Summary}

% \begin{table}
%   \begin{center}
%     \begin{tabular}{r|r|r|r|r}
%       configured memory size & 1~GB & 2~GB & 4~GB & 8~GB\\
%       \hline
%       suspend image size & 166~MB & 196~MB & 316~MB & 422~MB\\
%     \end{tabular}
%   \end{center}
%   \caption{.}
%   \label{tab:minimal-suspend-image}
% \end{table}

% Besides the initial resume latency we also evaluated the impact of our on-demand resume on the continued execution of the VM.
% Because we read the guest's memory from disk whenever it is needed,
% there number of major page faults will be higher than in the original implementation.
% Whenever the guest VM accesses a memory location for which the content has not yet been read from disk,
% a page fault is generated.
% The guest is briefly suspended while the host OS handles the transfer from disk to memory.
% Once the page fault handler returns, execution inside the guest OS can continue.

%% \note{TK: argue that page fault time only minimally increases response time;
%% wikipedia says its 8~ms for a page fault; average processing latency is XXXX~ms}

%% \note{TK: optimizations affect tail latencies more than they affect the average latency.
%% Cite some previous work here.}

%% \note{TK: start/stop loses cache state; resume preserves cache across on/off periods}

%% \note{TK: has to read page from file/disk even if (i)~it will be overwritten immediately, (ii)~contains zeroes}