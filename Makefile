FIGS=$(wildcard figs/*.pdf)
TEXS=$(wildcard *.tex)

paper.pdf : $(TEXS) $(FIGS) refs.bib
	rubber -f -d paper.tex

clean :
	$(RM) -f paper.aux paper.blg paper.log paper.out paper.pdf paper.bbl

.PHONY : clean
