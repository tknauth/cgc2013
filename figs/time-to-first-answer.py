import collections
import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import re
import fileinput
import numpy as np
from pylab import *

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
# pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def create_figure(ramcfg_mb) :

    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    f = open('1370377233.csv', 'r')
    keys = f.readline().strip().split() # skip header
    ys1 = []
    ys2 = []
    xs = []

    for line in f :
        line = line.strip()
        cols = line.split()

        d = {}
        for (k, v) in zip(keys, cols) :
            d[k] = v

        if d['ramcfg_mb'] == ramcfg_mb and d['qemu'] == 'stock' :
            ys1.append(d['time_s'])
            xs.append(int(d['dumpsize_byte'])/1024./1024./1024.)
        elif d['ramcfg_mb'] == ramcfg_mb and d['qemu'] == 'custom' :
            ys2.append(d['time_s'])

    plt.plot(xs, ys1, 'o-', color='#222222', markersize=4)
    plt.plot(xs, ys2, 'x--', color='#555555', markersize=4)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')
    ax1.set_ylim(0)
    ax1.set_xlim(0)
    # ax1.set_xscale('log')

    plt.xlabel('suspend image size [GB]')
    plt.ylabel('request latency [s]')
    plt.legend(['stock', 'custom'], loc=0, numpoints=1)

    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('time-to-first-answer-%s'%(ramcfg_mb))

    # plot(X, Y)
    # plot(X,CY,'r--')

create_figure('2048')
create_figure('4096')
