import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp
from collections import defaultdict

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(32/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def parse_input(filename) :
    vals = []

    f = open(filename)
    line = f.readline()
    columns = line.strip().split()

    for line in f :
        line = line.strip()
        fields = line.split()
        kv = {}
        for (k,v) in zip(columns, fields) :
            kv[k] = v
        vals.append(kv)
    return vals

def get_block_count(block_file) :
    p = sp.Popen('wc -l %s | cut -f1 -d" "'%(block_file), shell=True, stdout=sp.PIPE)
    (out, err) = p.communicate()
    return int(out.strip())

plot_order = ['stock', 'custom']
method2color={'stock' : '#AAAAAA', 'custom' : '#888888'}

# xs may be a list of list. This creates a multi-plot figure.
def plot_single_figure(bars, xlabel='', ylabel='') :
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    ind = np.arange(4)
    bar_width = 0.25
    rects = []
    for (i, k) in enumerate(plot_order) :
        # print map(np.mean, bars[k])
        print map(np.std, bars[k])
        plt.bar(ind+bar_width*i, map(np.mean, bars[k]), bar_width, color=method2color[k], label=k,
                linewidth=0.5)
        

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('none')
    ax1.yaxis.set_ticks_position('left')
    ax1.set_xticks(ind+bar_width)
    ax1.set_xticklabels([200,740,1200,1800])
    # ax1.set_yticks([6,12,18,24])
    # ax1.set_ylim(0, 24)
    ax1.set_xlim(-0.2, 3.7)
    # ax1.set_yscale('log')

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    plt.legend(bbox_to_anchor=(0.5, 1.1), loc="upper center", numpoints=1, ncol=5)
    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('scalability-bar')

vals = parse_input('1375887435-time-to-resume.csv')

# filter = ['ssd', '']
vals_filtered = filter(lambda kv : kv['disk'] == 'ssd', vals)

bars = defaultdict(list)
minmax = defaultdict(list)

for qemu in ['stock', 'custom'] :
    for ramuse_mb in [1,512,1024,1536] :
        vals_filtered = filter(lambda kv : kv['ramuse_mb'] == str(ramuse_mb), \
                                   filter(lambda kv : kv['disk'] == 'ssd', vals))
        vals_filtered = filter(lambda kv : kv['qemu'] == qemu, vals_filtered)
        vals_filtered = filter(lambda kv : kv['concurrent'] == '4', vals_filtered)

        individual_run_avg_resume_time_sec = []

        for run in [x['time_s'] for x in vals_filtered] :
            times_sec = map(float, run.split(','))
            individual_run_avg_resume_time_sec.append(np.mean(times_sec))

        bars[qemu].append(individual_run_avg_resume_time_sec)

# bars = {'stock' : [[3,4],[5,6],[7,8],[9,10]],
#         'custom' : [[4,5,2], [10,12,11], [13,13,13], [15,15]]}
print minmax

plot_single_figure(bars, 'suspend image size [MB]', 'time to first response [s]')
