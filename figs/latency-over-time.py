import collections
import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import pylab
import re
import fileinput
import numpy as np
from pylab import *
import time
import matplotlib.mlab as mlab
from scipy import stats

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(33/72.27)/width)
# pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

start_log = '20130606133945-start-access.log'
resume_log = '20130606133945-resume-access.log'

def parse_access_log(fname) :
    f = open(fname, 'r')

    time_offset = None
    xs = []
    ys = []

    for line in f :
        line = line.strip()
        fields = line.split()

        if not fields[3] == 'GET' : continue

        ys.append(int(fields[-1])/1000.)
        time_string = fields[0]+' '+fields[1]
        time_string = time_string.strip('"')
        if not time_offset :
            time_offset = time.strptime(time_string, '%Y-%m-%d %H:%M:%S')
            print time_offset
        t = time.strptime(time_string, '%Y-%m-%d %H:%M:%S')
        # print time
        # print time.mktime(time_offset), time.mktime(t)
        xs.append(time.mktime(t) - time.mktime(time_offset))

    return (xs, ys)

def create_time_series_plot() :

    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    marker = ['o', 'x']
    color  = ['#222222', '#555555']
    for (i, log) in enumerate([start_log, resume_log]) :
        (xs, ys) = parse_access_log(log)
        print 'min/max/avg', np.min(ys), np.max(ys), np.mean(ys)
        print '99th/95th/90th percentile', np.percentile(ys, 90)
        plt.plot(xs, ys, marker[i], color=color[i], markersize=2)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')
    ax1.set_ylim(0)
    ax1.set_xlim(0)
    # ax1.set_xscale('log')

    plt.xlabel('time [s]')
    plt.ylabel('request latency [us]')
    plt.legend(['start', 'resume'], loc=0, numpoints=1)

    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('latency-over-time')

    # plot(X, Y)
    # plot(X,CY,'r--')

def create_latency_histogram() :

    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    marker = ['o', 'x']
    color  = ['#222222', '#555555']
    for (i, log) in enumerate([start_log, resume_log]) :
        (xs, ys) = parse_access_log(log)
        n, bins, patches = ax1.hist(ys, 100, normed=1)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')
    ax1.set_ylim(0)
    ax1.set_xlim(0)
    # ax1.set_xscale('log')

    plt.xlabel('time [s]')
    plt.ylabel('request latency [us]')
    plt.legend(['stock'], loc=0, numpoints=1)

    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('latency-histogram')

    # plot(X, Y)
    # plot(X,CY,'r--')

def plot_cdf(xs) :
    d = collections.defaultdict(float)
    for x in xs :
        d[x] += 1

    xs = sort(d.keys())
    ys1 = [d[x] for x in xs]
    ys1 = np.array(ys1)
    ys1 /= np.sum(ys1)
    cys1 = np.cumsum(ys1)

    plt.plot(xs, cys1)

def create_latency_cdf() :

    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    marker = ['', '']
    color  = ['#222222', '#555555']
    for (i, log) in enumerate([start_log, resume_log]) :
        (xs, ys) = parse_access_log(log)
        plot_cdf(ys)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')
    ax1.set_ylim(0)
    ax1.set_xlim(0)
    ax1.set_xscale('log')

    plt.xlabel('latency [us]')
    plt.ylabel('CDF')
    plt.legend(['start', 'resume'], loc=0, numpoints=1)

    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('latency-cdf')

    # plot(X, Y)
    # plot(X,CY,'r--')


create_time_series_plot()
create_latency_histogram()
create_latency_cdf()
