import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import re
import glob
import sys

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 3.2
height = width/1.3

# pylab.rc("figure.subplot", left=(30/72.27)/width)
# pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def parse_input(input_file) :
    rows = []
    for line in open(input, 'r') :
        cols = line.strip().split()
        rows.append(cols)
    return rows

def create_figure(input) :
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    rows = parse_input(input)
    memory_sizes_mb = set([row[0] for row in rows])
    memory_sizes_mb = map(int, memory_sizes_mb)
    memory_sizes_mb = sorted(memory_sizes_mb)

    # filter out 8 GB instances
    memory_sizes_mb = memory_sizes_mb[0:3]

    marker = ['^', 'o', 'x', 'v']

    legend = []
    for (i, memory_size_mb) in enumerate(memory_sizes_mb) :
        xys = [(row[1],row[2]) for row in rows if int(row[0]) == memory_size_mb]
        xs = [int(xy[0])/1024/1024 for xy in xys]
        ys = [xy[1] for xy in xys]
        legend.append(str(int(memory_size_mb)/1024)+' GB')
        plt.plot(xs, ys, marker[i], color='#222222', markersize=4.0, label=str(memory_size_mb), clip_on=False)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')

    plt.xlabel('dump size [MB]')
    plt.ylabel('resume time [seconds]')
    plt.ylim([0,10])
    plt.legend(legend, loc=4, numpoints=1, title='instance RAM\nconfiguration')

    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    output_fn = input.replace('.csv', '')
    plt.savefig(output_fn, transparent=True)

input = sys.argv[1]
create_figure(input)
