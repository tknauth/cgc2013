\section{Related Work}

Work on suspending and resuming virtualized operating systems is more than a decade old~\cite{kozuch2002internet}. 
Werner Vogels mentioned the suspend/resume mechanism of virtual machines as one of many use case for virtualization technology~\cite{vogels2008beyond}.
He referred to it as ``application parking'' though.
For batch applications the resume time is uncritical.
However, for interactive, user-facing web applications, the resume time is essential to provide a satisfactory user experience.
For a web-based business, fast response times directly translate into more revenue.
For example, Amazon claims that for every 100~ms increase in page load time their sales decrease by 1\%.
If the idea of temporarily halting VMs is to find wide-spread adoption,
resuming the VMs has to be fast; definitely faster than today's default implementation.

Our work builds on ideas previously investigated in the related context of VM live migration.
Live migrating a virtual machine means transferring a VM to a new destination host with minimal service interruption.
There are two variants of live migration: pre- and post-copy.
With pre-copy migration~\cite{clark2005livemigration} the bulk of the VM's state is copied to the destination host \emph{before} starting to execute the VM on the destination host.
Conversely, with post-copy migration, the execution is started on the destination host as soon as possible.
Possibly missing state, required for the VM's execution, is transmitted on-demand from the source host.
For the pre-copy approach, the destination host has no dependencies on the source host, as soon as the instance starts executing on the destination;
the state transistion is complete when execution is started.
Post-copy migration allows to switch the actual execution much more quickly than with the pre-copy approach.
This is offset by additional latency as a result of on-demand paging of memory from the source host.
The pre-copy approach is implemented and shipped with qemu,
while a post-copy implementation is provided as an extension by~\citet{hirofuchi2010enabling}.
Besides the two basic approaches to migration, various optimizations
have been proposed~\cite{jin2009adaptive, svard2011evaluation} or the migration extended to also cover persistent storage~\cite{haselhorst2011efficient, bradford2007live}.

Besides copying the in-memory state of VMs,
\citet{liu2009trace} experimented with a trace and replay based migration system.
For the evaluated workload, important metrics, such as downtime and network traffic volume,
improved compared to traditional pre-copy approaches.

Conceptually, resuming a VM is similar to a live migration where the source and destination host are the same.
However, the live migration implementation is much more complex,
because is is designed for a generic distributed scenario.
The source and destination host communicate over the network, e.g., via TCP,
whereas resuming a VM only requires reading data from disk.
Thus, the appeal of our tailored implementation is its reduced complexity.
By changing fewer than 100 lines of code,
we are able to significantly decrease the resume time.
This is possible because we leverage the mechanisms already present in the host operating system for on-demand paging.

Among the related work, the Twinkle system~\cite{zhu2011twinkle} is most similar to ours.
While Twinkle uses Xen for virtualization, we based our work on the open-source emulator qemu.
Twinkle assumes a scenario where the instance image together with the execution snapshot is stored at a central location.
To reduce the number of remote page faults, Twinkle uses working set estimation.
Upon starting a VM, the entire estimated working set is transferred to the VM host.
Twinkle employs other optimizations to reduce the total number of pages to transfer.
This increases the implementation complexity to around 3500 lines of code.
Our implementation targets a different scenario where the VM's image and snapshot are stored locally.
Optimizing the page access pattern is less of a concern.
This reduces the implementation complexity by an order of magnitude; less than 100 lines of code.

In addition to our work based on the open-source emulator qemu,
commercial providers of virtualization solutions are also working to speed up the VM resume process~\cite{zhang2013esx, zhang2011workingset}.
\citeauthor{zhang2011workingset} estimate the VM's working set and show how the estimation improves the restore performance compared to an uninformed lazy restore.
We use different applications to evaluate our lazy restore implementation,
as well as different hardware (SSDs instead of HDDs),
which, we believe, provide additional interesting data points.
